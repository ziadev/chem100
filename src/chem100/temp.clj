(ns chem100.temp
  (import org.apache.chemistry.opencmis.client.api.ItemIterable)
  (import org.apache.chemistry.opencmis.client.api.Folder)
  (import org.apache.chemistry.opencmis.client.api.Document) 
  (:require [chem100.session :as session]
            [chem100.repository :as repo]
            [chem100.folder :as folder]
            [chem100.document :as doc]
            [chem100.cmisobject :as co]))
     
(use '[clojure.string :only (join split)])

; ToDo - CMIS Scripts as examples and CMIS.groovy
; Navigation Services
; Multi-filing Services
; Discovery Services
; Versioning Services
; Relationship Services
; Policy Services
; ACL (Access Control List) Services
 
(def in-mem-session 
  (session/create-session "http://localhost:8081/inmemory/atom/" "test" "test" "A1"))

; // Get everything in the root folder and print the names of the objects
;        Folder root = folder.getRootFolder();
;        ItemIterable<CmisObject> children = root.getChildren();
;        System.out.println("Found the following objects in the root folder:-");
;        for (CmisObject o : children) {
;            System.out.println(o.getName());
;        }
; we need to be able to write a recursive descent for processing folders

(defn looprecur_fibonacci [n]
  (loop [first 0 second 1 iter 0]
    (if (= iter n)
      first
      (recur second (+ first second) (inc iter)))))

(defn process-print-list [a-list]
  (cond 
   (empty? a-list)
   'false
   (list? a-list)
   (let [a a-list]
     (println (first a))
     (process-print-list (rest a)))
   :else
   (process-print-list (rest a-list))))

(comment 
  (defn process-my-list [list]
    (if (not (empty? list))
      (let [first (first list)]
        (println (co/get-name first))
        (process-print-my-list (rest list)))
      )))
(comment 
  (process-my-list (get-children-for-id "100")))

(defn my-print [item]
  (println (co/get-name item)))

(defn process-list [func a-list]
  (loop [loop-list a-list]
    (if (empty? loop-list)
      (println "done")
      (let [item (first loop-list)]
        (func item)
        (recur (rest loop-list))))))

(process-list my-print (get-children-for-id "100"))

(my-print (co/get-object in-mem-session "100"))


(defn get-children-for-id [id]
  (lazy-seq (folder/get-children
             (co/get-object in-mem-session id (folder/create-folder-context in-mem-session)))))


(defn get-children* [x]
  (loop [result ()
         children (get-children-for-id "100")]
    (if (seq children)
      (let [first (first children)
            rest (rest children)
            grand-children (get-children-for-id first)]
        (recur (conj result first)
               (if (seq grand-children)
                 (concat rest grand-children)
                 rest)))
      result)))


