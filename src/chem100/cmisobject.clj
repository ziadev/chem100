(ns chem100.cmisobject
  "CMIS Object Services"
  (import org.apache.chemistry.opencmis.client.api.CmisObject))

;CmisObject	getObject(ObjectId objectId)
;Returns a CMIS object from the session cache.

;CmisObject	getObject(ObjectId objectId, OperationContext context)
;Returns a CMIS object from the session cache.

;CmisObject	getObject(String objectId)
;Returns a CMIS object from the session cache.

;CmisObject	getObject(String objectId, OperationContext context)
;Returns a CMIS object from the session cache.

;CmisObject	getObjectByPath(String path)
;Returns a CMIS object from the session cache.

;CmisObject	getObjectByPath(String path, OperationContext context)
;Returns a CMIS object from the session cache.


(defn create-object-id [session id]
  "Creates an object id from a String->ObjectId"
  (. session createObjectId id))

(defn get-object-by-path
  ([session path]
    (. session getObjectByPath path))
  ([session context path]
    (. session getObjectByPath path context))
  )

(defn get-object
  "Returns a CMIS object from the session cache->CmisObject"
  ([session id]
     (. session getObject id))
  ([session id context]
     (. session getObject id context)))

(defn delete
  "Deletes an object and, if it is a document, all versions in the version series"
  ([session id]
     (. session delete id))
  ([session id all-versions]
     (. session delete id all-versions)))

(defn get-name [cmis-object]
  (let [name (. cmis-object getName)]
    name))

(defn get-object-id [cmis-object]
  (. cmis-object getId))

(defn get-base-type-id [cmis-object]
  (. cmis-object getBaseTypeId))

(defn get-object-type-id [cmis-object]
  "Doesn't seem to work on Folders"
  (. cmis-object getObjectTypeId))

(defn get-created-by [cmis-object]
  (. cmis-object getCreatedBy))

; cmis:createdBy
; cmis:creationDate
; cmis:lastModifiedBy
; cmis:lastModificationDate
; cmis:changeToken (String)

; this returns an #<UnmodifiableRandomAccessList [Property
; need to be able to parse those
; using bean only works for document, I think we can "parse" the list
(defn get-properties [cmis-object]
  (. cmis-object getProperties))

(defn get-property [cmis-object]
  (. cmis-object getProperty "NAME")
  )


