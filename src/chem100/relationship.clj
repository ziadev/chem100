(ns chem100.relationship
  (import org.apache.chemistry.opencmis.client.api.Relationship)
  (import org.apache.chemistry.opencmis.commons.enums.IncludeRelationships)
  (import org.apache.chemistry.opencmis.commons.enums.RelationshipDirection)
  (import org.apache.chemistry.opencmis.client.runtime.OperationContextImpl)
  (:require [chem100.session :as session]
            [chem100.cmisobject :as co]))

(defn create-relationship-operation-context []
  (doto (new OperationContextImpl)
        (.setIncludeRelationships IncludeRelationships/BOTH)))

;Creates a new relationship.
;ObjectId	createRelationship(Map<String,?> properties)
(defn create-relationship
  ([session properties]
   "Creates a Relationship using the values from properties"
    (. session createRelationship properties nil nil nil))
  ([session source-id target-id relationship]
   "Creates a Relationship using the values from properties"
     (let [properties (doto (new java.util.HashMap)
                        (.put "cmis:sourceId" source-id)
                        (.put "cmis:targetId" target-id)
                        (.put "cmis:objectTypeId" relationship))]
      (. session createRelationship properties nil nil nil))))

;Creates a new relationship.
;ObjectId	createRelationship(Map<String,?> properties, List<Policy> policies, List<Ace> addAces, List<Ace> removeAces)

;Fetches the relationships from or to an object from the repository.
;ItemIterable<Relationship>	getRelationships(ObjectId objectId, boolean includeSubRelationshipTypes, RelationshipDirection relationshipDirection, ObjectType type, OperationContext context)
(defn get-relationships [session object-id]
    (. session getRelationships (co/create-object-id session object-id) true RelationshipDirection/EITHER nil (create-relationship-operation-context)))

(defn get-relationships->seq [session object-id]
    (seq
     (. session
        getRelationships
        (co/create-object-id session object-id)
        true
        RelationshipDirection/EITHER
        nil
        (create-relationship-operation-context))))


;; Relationship Interface
;Gets the source object.
;CmisObject	getSource()
(defn get-source
  ([relationship]
  (. relationship getSource))
  ([relationship context]
  (. relationship getSource context)))

;Gets the source object using the given OperationContext.
;CmisObject	getSource(OperationContext context)

;Gets the target object.
;CmisObject	getTarget()
(defn get-target
  ([relationship]
  (. relationship getTarget))
  ([relationship context]
  (. relationship getTarget context)))

;Gets the target object using the given OperationContext.
;CmisObject	getTarget(OperationContext context)

;TransientRelationship	getTransientRelationship()
