
(ns chem100.fileops)
      
(defn create-file [path]
   (new java.io.File path))

(defn create-file-input-stream [file]
  (new java.io.FileInputStream file))

