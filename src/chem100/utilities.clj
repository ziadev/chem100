(ns chem100.utilities
  (import org.apache.chemistry.opencmis.client.api.ItemIterable)
 (import org.apache.chemistry.opencmis.client.api.Folder)
  (:require [chem100.session :as session]
            [chem100.repository :as repo]
            [chem100.folder :as folder]
            [chem100.document :as doc]
            [chem100.cmisobject :as co]))

(use '[clojure.string :only (join split)])

;List<Repository> repositories = factory.getRepositories(parameter);
;Session session = repositories.get(0).createSession()

(def repo-param (session/create-parameter "http://localhost:8080/alfresco/cmisatom" "admin" "admin"))

(def alfresco-session
  (session/create-session repo-param (repo/get-first-repository-id repo-param)))

(comment desktop localhost...
         (def alfresco-session
           (session/create-session "http://localhost:8080/alfresco/cmisatom/" "admin" "admin" "94eaa5e0-bbe4-49b8-9711-74a21ab2c474")))

(comment need Crud
         Create - pretty specific to Type, later
         Read -
         Update
         Delete)

(defn query [session search-all-versions query]
  (. session query query search-all-versions))

(defn get-property-by-query-name [result]
  (. (. result getPropertyByQueryName "cmis:name") getFirstValue))

(defn get-property-by-name [result]
  (. (. result getPropertyByQueryName "cmis:objectId") getFirstValue))

(defn get-property-by-person-id [result]
  (. (. result getPropertyByQueryName "dpp:personId") getFirstValue))

(defn get-property [field result]
  (. (. result getPropertyByQueryName field) getFirstValue))

(defn build-query [field type where-clause]
  (str "select " field " from " type " where " where-clause))

(println
  (build-query "dpp:personId" "dpp:person" "dpp:personId like 'g%") )

(defn run-cmis-query
  ([select-clause]
     (query alfresco-session false select-clause))
  ([field type where-clause]
     (let [sql-query (build-query field type where-clause)
           qr (query alfresco-session false sql-query)]
       (map (partial get-property field) qr))))

(defn create-object-id [object]
  (co/create-object-id alfresco-session
                       ( first
                         (split  object #";"))) )

(create-object-id "workspace://SpacesStore/baeb70da-cebc-496f-bda5-6ce0f94a194c;1.0" )

(comment
  (co/get-object alfresco-session (co/create-object-id alfresco-session "workspace://SpacesStore/baeb70da-cebc-496f-bda5-6ce0f94a194c;1.0") ))

(co/get-name
 (co/get-object-by-path alfresco-session "/Configuration/Instance Data/Persons"))

(println
  (let [qr (query alfresco-session false "select dpp:personId from dpp:person")]
  (map get-property-by-person-id (take 5 qr))))

(println
  (run-cmis-query "select * from dpp:articleReferenceFolder"))

(type (first
       (let [qr (query alfresco-session false "select cmis:objectId from dpp:person where dpp:personId like 'g%'")]
         (map (partial  get-property "cmis:objectId") qr))))

(run-cmis-query "dpp:personId" "dpp:person" "dpp:personId like 'g%'")
(run-cmis-query "dpp:personId" "dpp:person" "dpp:legacyPersonId = '13' or dpp:legacyPersonId = '660'")
(run-cmis-query "dpp:personId" "dpp:person" "dpp:personId like 'PublicationId%'")
(sort  (run-cmis-query "cmis:name" "dpp:article" "cmis:name like '%'"))
(run-cmis-query "cmis:name" "dpp:article" "cmis:name like '%'")
(count  (run-cmis-query "cmis:name" "dpp:article" "cmis:name like '%'"))
(run-cmis-query "cmis:name" "dpp:article" "cmis:name like '1%'")
(run-cmis-query "cmis:name" "dpp:articleReferenceFolder" "cmis:name like '1%'")

(comment
  (println
   (map (partial  co/create-object-id alfresco-session)
        ( run-cmis-query "cmis:objectId" "dpp:person" "dpp:personId like 'g%'"))))

(comment
  (co/get-object alfresco-session
                 (co/create-object-id alfresco-session "workspace://SpacesStore/baeb70da-cebc-496f-bda5-6ce0f94a194c")))

(comment
  (.
   (co/get-object
    alfresco-session
    (first
     (map (partial  co/create-object-id alfresco-session)
          (run-cmis-query "cmis:objectId" "dpp:person" "dpp:personId like 'gc%'")))) delete))

(comment
  (co/get-object alfresco-session
                 ( run-cmis-query "cmis:objectId" "dpp:person" "dpp:personId like 'gc%'")))

;(folder/get-root-folder alfresco-session)
;(last (. (folder/get-root-folder alfresco-session) getChildren))
;(count (co/get-properties (last (. (folder/get-root-folder alfresco-session) getChildren))))
;(first (map co/get-object-id (. (folder/get-root-folder alfresco-session) getChildren)))
;(folder/create-folder  (folder/get-root-folder alfresco-session) "Lightning Talk")
;(co/get-object-by-path alfresco-session "/Lightning Talk")
;(. (co/get-object-by-path alfresco-session "/Lightning Talk") delete)




(comment
  (let [qr (query alfresco-session false "select cmis:name from dpp:person")]
    (map get-property-by-query-name (take 5 qr)))
  (let [qr (query alfresco-session false "select cmis:objectId from dpp:person")]
    (map get-property-by-name (take 5 qr))))

(comment root folder workspace://SpacesStore/f363825e-47d9-45d4-b48e-cb9aea4a8fc9
         (get-children-for-id alfresco-session "workspace://SpacesStore/f363825e-47d9-45d4-b48e-cb9aea4a8fc9")
         (co/get-object alfresco-session "workspace://SpacesStore/f363825e-47d9-45d4-b48e-cb9aea4a8fc9")
         (count  (map co/get-name (get-children-for-id alfresco-session "workspace://SpacesStore/f363825e-47d9-45d4-b48e-cb9aea4a8fc9")))
         (map co/get-name (get-children-for-id alfresco-session "workspace://SpacesStore/f363825e-47d9-45d4-b48e-cb9aea4a8fc9"))
         )

(comment
    (let [qr (query in-mem-session false "select cmis:name, cmisbook:author from cmisbook:lyrics")]
      (map get-property-by-query-name (take 5 qr))))

(comment
  (let [qr (query in-mem-session false "select cmis:name from cmisbook:lyrics")]
    (map get-property-by-query-name qr)))

(comment
  (query in-mem-session false "select cmis:objectId from cmisbook:lyrics"))

(defn test-fn [result]
  (. (. result getPropertyByQueryName "cmis:objectId") getFirstValue))

(comment
  (let [qr (query alfresco-session false "select cmis:objectId from cmis:document")]
    (map test-fn (seq qr))))

(defn run-query [session cmis-query]
  (seq (query session false cmis-query)))

(comment
  (run-query alfresco-session "select cmis:objectId from cmis:document")
  (count (run-query alfresco-session "select cmis:objectId from cmis:document"))
  (let [qr (query alfresco-session false "select cmis:objectId from cmis:document")]
    (map test-fn (seq qr))))

(comment
  (count (run-query alfresco-session "select cmis:objectId from cmis:folder"))
  (let [qr (query alfresco-session false "select cmis:objectId from cmis:folder")]
    (map test-fn (seq qr))))

(defn get-children-for-id [session id]
  (let [the-object (co/get-object session id)]
    (if (instance? Folder the-object)
      (lazy-seq (folder/get-children the-object))
      ())
    ))

(comment
  (get-children-for-id alfresco-session
                       (first
                        (let [children (get-children-for-id alfresco-session "workspace://SpacesStore/f363825e-47d9-45d4-b48e-cb9aea4a8fc9")]
                          (map co/get-object-id children)))))

(defn get-children [session id]
  (loop [result ()
         children (get-children-for-id session id)]
    (if (seq children)
      (let [first (first children)
            rest (rest children)
            grand-children (get-children-for-id session first)]
        (recur (conj result first)
               (if (seq grand-children)
                 (concat rest grand-children)
                 rest)))
      result)))

(defn format-item [item]
  (let [object-id (co/get-object-id item)
        name (co/get-name item)
        type (co/get-base-type-id item)]
    (str object-id ":" name ":" type)))

(defn get-sorted-list []
  (sort (map format-item (get-children alfresco-session "workspace://SpacesStore/f363825e-47d9-45d4-b48e-cb9aea4a8fc9"))))

(defn get-list []
  (get-children alfresco-session "workspace://SpacesStore/f363825e-47d9-45d4-b48e-cb9aea4a8fc9"))

(comment
  (println
   (map format-item (get-children alfresco-session "workspace://SpacesStore/f363825e-47d9-45d4-b48e-cb9aea4a8fc9"))))

;(sort (map format-item (get-children alfresco-session "workspace://SpacesStore/f363825e-47d9-45d4-b48e-cb9aea4a8fc9")))
;(count (map format-item (get-children alfresco-session "workspace://SpacesStore/f363825e-47d9-45d4-b48e-cb9aea4a8fc9")))


