(ns chem100.session
  (import org.apache.chemistry.opencmis.client.api.Session)
  (import org.apache.chemistry.opencmis.client.runtime.SessionFactoryImpl)
  (import org.apache.chemistry.opencmis.commons.SessionParameter)
  (import org.apache.chemistry.opencmis.commons.enums.BindingType)
  (import org.apache.chemistry.opencmis.commons.enums.IncludeRelationships)
  (:require [chem100.fileops :as fo]))

(defmacro with-session [& body]
  `(sql/with-connection {:classname "org.h2.Driver"
                         :subprotocol "h2"
                         :subname "mem:"}
     ~@body))

;; does this work with 4.2?
;; probably needs a different URL
(defn create-parameter [url userid password]
  "Sample URLs \"http://<host>:<port>/cmis/atom\" \"http://repo.opencmis.org/inmemory/atom/\"
   \"http://localhost:8081/inmemory/atom/\")"
  (doto (new java.util.HashMap)
    (.put SessionParameter/ATOMPUB_URL url)
    (.put SessionParameter/USER, userid)
    (.put SessionParameter/PASSWORD, password)
    (.put SessionParameter/BINDING_TYPE, (. BindingType/ATOMPUB value))))

(defn session-factory []
  (. SessionFactoryImpl newInstance))

(defn create-session
  ([url userid password repository-id]
     (let [param (create-parameter url userid password)]
       (. param put SessionParameter/REPOSITORY_ID repository-id)
       (. (. SessionFactoryImpl newInstance) createSession param)))
  ([created-param repository-id]
     (let [param created-param]
       (. param put SessionParameter/REPOSITORY_ID repository-id)
       (. (. SessionFactoryImpl newInstance) createSession param))))

(defn get-object-factory [session]
  (. session getObjectFactory))

(defn create-operation-context [session]
  (. session createOperationContext))

(defn get-default-context [session]
  (. session getDefaultContext))

(defn set-default-context! [session context]
  (. session setDefaultContext context))

(defn set-max-items-per-page! [context count]
  (doto context
    (.setMaxItemsPerPage count)))

(defn set-order-by! [context value]
  (doto context
    (.setOrderBy value)))

(defn create-content-stream [session filename mimetype]
  (let [file (fo/create-file filename)
        fis (fo/create-file-input-stream file)]
    (. (get-object-factory session) createContentStream (. file getName) (. file length) mimetype fis)))

(defn create-document-context-content [session]
  (doto (create-operation-context session)
    (.setFilterString "cmis:objectId, cmis:baseTypeId, cmis:name, cmis:contentStreamLength, cmis:contentStreamMimeType")
    (.setIncludeAcls false)
    (.setIncludeAllowableActions true)
    (.setIncludePolicies false)
    (.setIncludeRelationships IncludeRelationships/NONE)
    (.setRenditionFilterString "cmis:none")
    (.setIncludePathSegments false)
    (.setOrderBy "cmis:name")
    (.setCacheEnabled false)
    (.setMaxItemsPerPage 10)))

(defn create-document-context-thumbnail [session]
  (doto (create-operation-context session)
    (.setFilterString "*")
    (.setIncludeAcls false)
    (.setIncludeAllowableActions true)
    (.setIncludePolicies false)
    (.setIncludeRelationships IncludeRelationships/NONE)
    (.setRenditionFilterString "cmis:thumbnail")
    (.setIncludePathSegments false)
    (.setOrderBy nil)
    (.setCacheEnabled true)))

(comment
  (set-max-items-per-page!
   (set-order-by! (create-operation-context in-mem-session) "cmis:name")
   10))

(defn clear [session]
  "Clears all cached data"
  (. session clear))

;OperationContext	createOperationContext()
;Creates a new operation context object.

;OperationContext	createOperationContext(Set<String> filter, boolean includeAcls, boolean includeAllowableActions, boolean includePolicies, IncludeRelationships includeRelationships, Set<String> renditionFilter, boolean includePathSegments, String orderBy, boolean cacheEnabled, int maxItemsPerPage)
;Creates a new operation context object with the given properties.

;CmisBinding	getBinding()
;Returns the underlying binding object.

;ChangeEvents	getContentChanges(String changeLogToken, boolean includeProperties, long maxNumItems)
;Returns the content changes.
;ChangeEvents	getContentChanges(String changeLogToken, boolean includeProperties, long maxNumItems, OperationContext context)
;Returns the content changes.

;Locale	getLocale()
;Get the current locale to be used for this session.

;ObjectFactory	getObjectFactory()
;Gets a factory object that provides methods to create the objects used by this API.

;void	removeObjectFromCache(ObjectId objectId)
;Removes the given object from the cache.

;void	removeObjectFromCache(String objectId)
;Removes the given object from the cache.

;OperationContext	getDefaultContext()
;Returns the current default operation parameters for filtering, paging and caching.

;void	setDefaultContext(OperationContext context)
;Sets the current session parameters for filtering, paging and caching.
