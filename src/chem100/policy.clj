(ns chem100.policy
  "Policy Services"
  (:require [chem100.session :as session]))

;void	applyPolicy(ObjectId objectId, ObjectId... policyIds) 
;Applies a set of policies to an object.

;ObjectId	createPolicy(Map<String,?> properties, ObjectId folderId)
;Creates a new policy.

;ObjectId	createPolicy(Map<String,?> properties, ObjectId folderId, List<Policy> policies, List<Ace> addAces, List<Ace> removeAces)
;Creates a new policy.

;void	removePolicy(ObjectId objectId, ObjectId... policyIds)
;Removes a set of policies from an object.
