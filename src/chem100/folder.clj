(ns chem100.folder
  (import org.apache.chemistry.opencmis.client.api.Folder)
  (import org.apache.chemistry.opencmis.commons.enums.IncludeRelationships)
  (import org.apache.chemistry.opencmis.commons.enums.UnfileObject)
  (:require [chem100.session :as session]))

(defn get-root-folder [session]
  (. session getRootFolder))

(defn root-folder? [folder]
  (. folder isRootFolder))

(defn get-parents [folder]
  (. folder getParents))

;; need to be able to pass in objectTypeId
(defn create-folder-props
  [folder-name]
  (doto (new java.util.HashMap)
    (.put "cmis:objectTypeId" "cmis:folder")
    (.put "cmis:name" folder-name)))

;; need to be able to pass in props
(defn create-folder
  ([parent folder-name]
    (. parent createFolder (create-folder-props folder-name))))

(defn create-folder-with-props [parent folder-props]
    (. parent createFolder folder-props))

;    public List<String> deleteTree(CmisObject folder, String folderId,
;                                   boolean allversions, UnfileObject unfile, boolean continueOnFailure) {
;        validateObjectOrId(folder, folderId);
;        validateRedundantIdentifier(folder, folderId);
;        CmisObject target = getCmisObject(folder, folderId);
;        if (target != null && target instanceof Folder) {
;            return ((Folder) target).deleteTree(allversions, unfile, continueOnFailure);
;        }
;        return null;
;    }
(defn delete-tree
  ([all-versions unfile continue-on-failure folder]
    (. folder deleteTree all-versions unfile continue-on-failure))
  ([folder]
    (. folder deleteTree true UnfileObject/DELETE true))
  )

(defn get-children
  ([folder]
     (. folder getChildren)
     )
  ([folder context]
     (. folder getChildren context)))

(defn get-page [children skip size]
  (. (. children skipTo skip) getPage size ) )

(defn create-folder-context [session]
  (doto (session/create-operation-context session)
    (.setFilterString "cmis:name, cmis:path")
    (.setIncludeAcls false)
    (.setIncludeAllowableActions false)
    (.setIncludePolicies false)
    (.setIncludeRelationships IncludeRelationships/NONE)
    (.setRenditionFilterString "cmis:none")
    (.setIncludePathSegments false)
    (.setOrderBy nil)
    (.setCacheEnabled true)))

; need to walk the tree of descendants recursively.
; getChildren() - gets just the direct containees in a folder
; getDescentants() - gets the containees of a folder and all their children to a specified depth
; getDescendants() -
; getFolderTree() - gets the set of descendant folder objects contained in a specified folder
; getFolderParent() - gets the parent folde object for the specified folder
; getObjectParents() - gets the parent folder(s) for the specified non-folder object

;Creates a new folder.
;ObjectId	createFolder(Map<String,?> properties, ObjectId folderId)

;Creates a new folder.
;ObjectId	createFolder(Map<String,?> properties, ObjectId folderId, List<Policy> policies, List<Ace> addAces, List<Ace> removeAces)

;Gets the root folder of the repository with the given OperationContext.
;Folder	getRootFolder(OperationContext context)

