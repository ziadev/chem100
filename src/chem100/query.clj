(ns chem100.query
  (import org.apache.chemistry.opencmis.client.api.ItemIterable)
  (:require [chem100.session :as session]
            [chem100.repository :as repo]
            [chem100.folder :as folder]
            [chem100.document :as doc]
            [chem100.cmisobject :as co]))
(use '[clojure.string :only (join split)])

;QueryStatement	createQueryStatement(String statement)
;Creates a query statement.

;ItemIterable<CmisObject>	queryObjects(String typeId, String where, boolean searchAllVersions, OperationContext context)

;Sends a query to the repository.
;ItemIterable<QueryResult>	query(String statement, boolean searchAllVersions)
;Sends a query to the repository using the given OperationContext.
;ItemIterable<QueryResult>	query(String statement, boolean searchAllVersions, OperationContext context)
(defn query
  ([session search-all-versions query]
    (. session query query search-all-versions))
  ([session search-all-versions query operation-context]
    (. session query query search-all-versions operation-context)))

(defn query->seq
  ([session cmis-query]
    (seq (query session false cmis-query)))
  ([session cmis-query operation-context]
    (seq (query session false cmis-query operation-context)))
  )

(defn get-property-by-query-name [result]
  (. (. result getPropertyByQueryName "cmis:name") getFirstValue))

(defn get-property-by-name [result]
  (. (. result getPropertyByQueryName "cmis:objectId") getFirstValue))

(defn get-property-by-status [result]
  (. (. result getPropertyByQueryName "dpp:status") getFirstValue))

(defn get-property-by-field [result field]
  (. (. result getPropertyByQueryName field) getFirstValue))

