(ns chem100.document)

;ObjectId	createDocumentFromSource(ObjectId source, Map<String,?> properties, ObjectId folderId, VersioningState versioningState)
;Creates a new document from a source document.

;ObjectId	createDocumentFromSource(ObjectId source, Map<String,?> properties, ObjectId folderId, VersioningState versioningState, List<Policy> policies, List<Ace> addAces, List<Ace> removeAces)
;Creates a new document from a source document.

;ItemIterable<Document>	getCheckedOutDocs()
;Returns all checked out documents.

;ItemIterable<Document>	getCheckedOutDocs(OperationContext context)
;Returns all checked out documents with the given OperationContext .

;ContentStream	getContentStream(ObjectId docId)
;Retrieves the main content stream of a document

;ContentStream	getContentStream(ObjectId docId, String streamId, BigInteger offset, BigInteger length)
;Retrieves the content stream of a document


(defn create-doc-props [folder-name]
  (doto (new java.util.HashMap)
    (.put "cmis:objectTypeId" "cmis:document")
    (.put "cmis:name" folder-name)))


;ObjectId	createDocument(Map<String,?> properties, ObjectId folderId, ContentStream contentStream, VersioningState versioningState)
; Creates a new document.

;ObjectId	createDocument(Map<String,?> properties, ObjectId folderId, ContentStream contentStream, VersioningState versioningState, List<Policy> policies, List<Ace> addAces, List<Ace> removeAces)
;Creates a new document.

(defn create-document
  ([folder doc-name]
     (. folder createDocument (create-doc-props doc-name) nil nil))
  ([folder doc-name content-stream]
     (. folder createDocument (create-doc-props doc-name) content-stream nil)))

(defn create-document-with-props
  ([folder props]
     (. folder createDocument props nil nil))
  ([folder props content-stream]
     (. folder createDocument props content-stream nil)))


;Creates a copy of this document, including content.
;Document 	copy(ObjectId targetFolderId)
;Creates a copy of this document, including content.

;Document 	copy(ObjectId targetFolderId, Map<String,?> properties, VersioningState versioningState, List<Policy> policies, List<Ace> addACEs, List<Ace> removeACEs, OperationContext context)

(defn copy
  ([document target-folder-id]
   (. document copy target-folder-id)))

(defn content? [document]
  (>= (. document getContentStreamLength) 0))

(defn get-content [document]
  (let [content (. document getContentStream)]
    (list
     (. content getStream)
     (. content getFileName)
     (. content getMimeType)
     (. content getLength))))

(defn get-renditions [document]
  (. document getRenditions))


; 2.4.3 Listing a Documents Properties - Listing 2.3

;cmis:isImmutable - Boolean
;cmis:isLatestVersion - Boolean
;cmis:MajorVersion - Boolean
;cmis:versionLabel - String
;cmis:versionSeriesId - ID
;cmis:isVersionSeriesCheckedOut - Boolean
;cmis:versionSeriesCheckedOutBy - String
;cmis:versionSeriesCheckedOutId - ID
;cmis:checkInComment - String
;cmis:contentStreamLength - Integer
;cmis:contentStreamMimeType - String
;cmis:contentStreamFileName - String
;cmis:contentStreamId - ID


;(content? (co/get-object in-mem-session "310"))
;(get-content (co/get-object in-mem-session "123"))
;(get-renditions (co/get-object in-mem-session "100"))

;(doc/create-document (folder/get-root-folder in-mem-session) "My Test Document")
;(. (new java.io.File "./purchase_order1.pdf") getName)
;(co/get-base-type-id (session/get-object-by-path in-mem-session "/My Test Folder/purchase_order1"))
;(doc/create-document "purchase_order1" (session/create-content-stream in-mem-session "./purchase_order1.pdf" "application/pdf"))

