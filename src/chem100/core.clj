(ns chem100.core
  (import org.apache.chemistry.opencmis.client.api.ItemIterable)
  (import org.apache.chemistry.opencmis.client.api.Folder)
  (import org.apache.chemistry.opencmis.client.api.Document) 
  (:require [chem100.session :as session]
            [chem100.repository :as repo]
            [chem100.folder :as folder]
            [chem100.document :as doc]
            [chem100.cmisobject :as co]))
     
(use '[clojure.string :only (join split)])

; ToDo - CMIS Scripts as examples and CMIS.groovy
; Navigation Services
; Multi-filing Services
; Discovery Services
; Versioning Services
; Relationship Services
; Policy Services
; ACL (Access Control List) Services
 
(def in-mem-session 
  (session/create-session "http://localhost:8081/inmemory/atom/" "test" "test" "A1"))

; // Get everything in the root folder and print the names of the objects
; Folder root = folder.getRootFolder();
; ItemIterable<CmisObject> children = root.getChildren();
; System.out.println("Found the following objects in the root folder:-");
; for (CmisObject o : children) {
;     System.out.println(o.getName());
; }
; we need to be able to write a recursive descent for processing folders

(defn get-children-for-id [session id] 
  (let [the-object (co/get-object session id)] 
    (if (instance? Folder the-object)
      (lazy-seq (folder/get-children the-object))
      ())
    ))

(get-children-for-id in-mem-session "100")
(get-children-for-id in-mem-session "123")

(type
 (co/get-base-typeid
  (co/get-object in-mem-session "123")))

(count  (map co/get-name (get-children-for-id in-mem-session "100")))
(map co/get-name (get-children-for-id in-mem-session "100"))

(get-children-for-id in-mem-session
 (first 
  (let [children (get-children-for-id in-mem-session "100")] 
    (map co/get-object-id children))))

(defn get-children [session id]
  (loop [result ()
         children (get-children-for-id session id)]
    (if (seq children)
      (let [first (first children)
            rest (rest children)
            grand-children (get-children-for-id session first)]
        (recur (conj result first)
               (if (seq grand-children)
                 (concat rest grand-children)
                 rest)))
      result)))

(defn format-item [item] 
  (let [object-id (co/get-object-id item)
        name (co/get-name item)
        type (co/get-base-type-id item)]
    (str object-id ":" name ":" type)))

(defn get-sorted-list []
  (sort (map format-item (get-children in-mem-session "100"))))

(defn get-list []
  (get-children in-mem-session "100"))
