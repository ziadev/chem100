(ns chem100.acl)

;Acl	applyAcl(ObjectId objectId, List<Ace> addAces, List<Ace> removeAces, AclPropagation aclPropagation) 
;Applies ACL changes to an object and potentially dependent objects.

;Acl	getAcl(ObjectId objectId, boolean onlyBasicPermissions)
;Fetches the ACL of an object from the repository.

;Acl	setAcl(ObjectId objectId, List<Ace> aces)
;Removes the direct ACEs of an object and sets the provided ACEs.


