(ns chem100.session-test
  (import org.apache.chemistry.opencmis.client.api.Folder)
  (import org.apache.chemistry.opencmis.client.api.Document) 
  (import org.apache.chemistry.opencmis.client.api.SessionFactory) 
  (:require [chem100.session :as session]
            [chem100.folder :as folder]
            [chem100.cmisobject :as co])
  (:use midje.sweet))

(with-state-changes [(around :facts (let [test-session (session/create-session "http://localhost:8081/inmemory/atom/" "test" "test" "A1")] ?form)
                             )]
                             (fact "getContentStreamLength" (. (co/get-object test-session "310") getContentStreamLength) => 395)
                             (fact "is-document" (instance? Document (co/get-object test-session "310")) => true)
                             (fact "is-folder" (instance? Folder (co/get-object test-session "100")) => true) 
                             (fact "get root folder" (instance? Folder (. test-session folder/getRootFolder)) => true)
                             (fact "SessionFactory" (instance? SessionFactory (session/session-factory)) => true)
)
