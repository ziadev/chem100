(ns chem100.relationship-test
  (import org.apache.chemistry.opencmis.client.api.Folder)
  (import org.apache.chemistry.opencmis.client.api.Document)
  (import org.apache.chemistry.opencmis.client.api.SessionFactory)
  (import org.apache.chemistry.opencmis.commons.PropertyIds)
   (import org.apache.chemistry.opencmis.commons.enums.RelationshipDirection)
  (:require [chem100.session :as session]
            [chem100.folder :as folder]
            [chem100.cmisobject :as co]
            [chem100.document :as doc]
            [chem100.relationship :as rel]
            [chem100.ql :as ql]))
(use '[clojure.string :only (join split)])

(ql/connect "connect to http://localhost:8080/alfresco/cmisatom user admin password admin")
ql/session_map

(keys (deref ql/session_map))
(vals (deref ql/session_map))

(def alfresco-session
  (first (vals (deref ql/session_map))))
alfresco-session

(ql/exec-select "select cmis:objectId from dpp:person where cmis:name like 'Test%'")
(ql/exec-delete "delete from dpp:person where cmis:name like 'Test%'")
(ql/exec-insert "insert document into path '/' (cmis:name, cmis:objectTypeId, dpp:personId) values ('Test1', 'D:dpp:person', 'mstang1@ziaconsulting.com')")
(ql/exec-insert "insert document into path '/' (cmis:name, cmis:objectTypeId, dpp:personId) values ('Test2', 'D:dpp:person', 'mstang2@ziaconsulting.com')")

;dpp:personToArticleEditorAssociation
(def person-id (first (vals (first (ql/exec-select "select cmis:objectId from dpp:person where dpp:personId = 'mstang1@ziaconsulting.com'")))))
person-id
(def article-id (first (vals (first (ql/exec-select "select cmis:objectId from dpp:article where cmis:name = '1230003'")))))
article-id
(ql/exec-select "select cmis:path from dpp:article where cmis:name = '1230003'")
;(ql/exec-select "select cmis:name from dpp:article where cmis:path = '/Editorial/2012/Sep/1230003'")
(split "/Editorial/2012/Sep/1230003" #"/")

(rel/create-relationship alfresco-session person-id article-id "R:dpp:personToArticleEditorAssociation")

person-id
(def object-id (co/create-object-id alfresco-session person-id))
object-id

(rel/get-relationships->seq alfresco-session person-id)
(first (rel/get-relationships->seq alfresco-session person-id))
(. (first (rel/get-relationships->seq alfresco-session person-id)) delete)
(rel/get-target (first (rel/get-relationships->seq alfresco-session person-id)))
(rel/get-source (first (rel/get-relationships->seq alfresco-session person-id)))
(. (first (rel/get-relationships->seq alfresco-session person-id)) getProperties)

(comment
    HashMap<String, Object> prop1 = new HashMap<String, Object>();
		prop1.put(PropertyIds.NAME , "Test 1");
		prop1.put(PropertyIds.OBJECT_TYPE_ID, "D:ws:article");

		HashMap<String, Object> prop2 = new HashMap<String, Object>();
		prop2.put(PropertyIds.NAME , "Test 2");
		prop2.put(PropertyIds.OBJECT_TYPE_ID, "D:ws:article");

		Folder folder = (Folder) session.getObjectByPath("/");

		Document doc1 = folder.createDocument(prop1, null, null, null, null, null, session.getDefaultContext());
		Document doc2 = folder.createDocument(prop2, null, null, null, null, null, session.getDefaultContext());

		Map<String, String> relProps = new HashMap<String, String>();
		relProps.put("cmis:sourceId", doc1.getId());
		relProps.put("cmis:targetId", doc2.getId());
		relProps.put("cmis:objectTypeId", "R:ws:relatedArticles");
		session.createRelationship(relProps, null, null, null);
  )
