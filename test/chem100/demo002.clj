(ns chem100.alfrescoSummit2013.demo002
  (import org.apache.chemistry.opencmis.client.api.ItemIterable)
  (:require [chem100.session :as session]
            [chem100.repository :as repo]
            [chem100.cmisobject :as co]))
(use '[clojure.string :only (join split)])

;; let's connect to our local repo
(def repo-param (session/create-parameter "http://localhost:8080/alfresco/cmisatom" "admin" "admin"))
(def alfresco-session
  (session/create-session repo-param (repo/get-first-repository-id repo-param)))

;; ------------------------------------------------------------------------
;; let's define some functions
(defn query [session search-all-versions query]
  (. session query query search-all-versions))

(defn get-property-by-query-name [result]
  (. (. result getPropertyByQueryName "cmis:name") getFirstValue))

(defn get-property-by-name [result]
  (. (. result getPropertyByQueryName "cmis:objectId") getFirstValue))

(defn get-property-by-person-id [result]
  (. (. result getPropertyByQueryName "dpp:personId") getFirstValue))

(defn get-property [field result]
  (. (. result getPropertyByQueryName field) getFirstValue))

(defn build-query [field type where-clause]
  (str "select " field " from " type " where " where-clause))

(defn create-object-id [object]
  (co/create-object-id alfresco-session
                       ( first
                         (split  object #";"))) )

;; let's define a function a Field like dpp:personId, a Type like dpp:person and a where-clause
(defn run-cmis-query
  ([field type where-clause]
     (let [sql-query (build-query field type where-clause)
           qr (query alfresco-session false sql-query)]
       (map (partial get-property field) qr))))

;; ------------------------------------------------------------------------
;; let's see the the a's sorted
;(println (sort (run-cmis-query "dpp:personId" "dpp:person" "dpp:personId like 'a%'")))

;; let's get a count of the a's
;(println (count (run-cmis-query "dpp:personId" "dpp:person" "dpp:personId like 'a%'")))

;(println (sort (run-cmis-query "dpp:manuscriptNumber" "dpp:article" "dpp:manuscriptNumber is not null")))
;(println (count (run-cmis-query "dpp:manuscriptNumber" "dpp:article" "dpp:manuscriptNumber is not null")))
