(ns chem100.query-test
  (import org.apache.chemistry.opencmis.client.api.ItemIterable)
  (:require [chem100.session :as session]
            [chem100.repository :as repo]
            [chem100.folder :as folder]
            [chem100.document :as doc]
            [chem100.cmisobject :as co]))

(use '[clojure.string :only (join split)])


;QueryStatement	createQueryStatement(String statement)
;Creates a query statement.

;ItemIterable<QueryResult>	query(String statement, boolean searchAllVersions)
;Sends a query to the repository.

;ItemIterable<QueryResult>	query(String statement, boolean searchAllVersions, OperationContext context)
;Sends a query to the repository using the given OperationContext.

;ItemIterable<CmisObject>	queryObjects(String typeId, String where, boolean searchAllVersions, OperationContext context)

(comment
  (def in-mem-session
    (session/create-session "http://localhost:8081/inmemory/atom/" "test" "test" "A1")))

(comment
  (def alfresco-session
    (session/create-session "http://localhost:8080/alfresco/cmisatom/" "admin" "admin" "0437a5e3-b54e-48da-8e76-d3e705271c3f")))

;; desktop localhost...
(comment
  (def alfresco-session
    (session/create-session "http://localhost:8080/alfresco/cmisatom/" "admin" "admin" "94eaa5e0-bbe4-49b8-9711-74a21ab2c474")))


(comment
  (def in-mem-session
    (session/create-session "http://localhost:8081/inmemory/atom/" "test" "test" "A1")))

(def alfresco-session
  (session/create-session "http://localhost:8080/alfresco/cmisatom/" "admin" "admin" "0437a5e3-b54e-48da-8e76-d3e705271c3f"))

;; desktop localhost...
(comment
  (def alfresco-session
    (session/create-session "http://localhost:8080/alfresco/cmisatom/" "admin" "admin" "94eaa5e0-bbe4-49b8-9711-74a21ab2c474")))

(defn query [session search-all-versions query]
  (. session query query search-all-versions))

(defn get-property-by-query-name [result]
  (. (. result getPropertyByQueryName "cmis:name") getFirstValue))

(defn get-property-by-name [result]
  (. (. result getPropertyByQueryName "cmis:objectId") getFirstValue))

(defn get-property-by-status [result]
  (. (. result getPropertyByQueryName "dpp:status") getFirstValue))


(comment
  (let [qr (query in-mem-session false "select cmis:name, cmisbook:author from cmisbook:lyrics")]
    (map get-property-by-query-name (take 5 qr))))

(comment
  (let [qr (query in-mem-session false "select cmis:name from cmisbook:lyrics")]
    (map get-property-by-query-name qr)))

(comment
  (query in-mem-session false "select cmis:objectId from cmisbook:lyrics"))

(defn test-fn [result]
  (. (. result getPropertyByQueryName "cmis:objectId") getFirstValue))


(let [qr (query alfresco-session false "select cmis:objectId from cmis:document")]
  (map test-fn (seq qr)))

(defn run-query [session cmis-query]
  (seq (query session false cmis-query)))

(count (run-query alfresco-session "select cmis:objectId from cmis:document"))

(run-query alfresco-session "select cmis:objectId from cmis:document")

(let [qr (query alfresco-session false "select cmis:objectId from cmis:document")]
  (map test-fn (seq qr)))

(count (run-query alfresco-session "select cmis:objectId from cmis:document"))

(count (run-query alfresco-session "select cmis:objectId from cmis:folder"))

(let [qr (query alfresco-session false "select cmis:objectId from cmis:folder")]
  (map test-fn (seq qr)))

(let [qr (query alfresco-session false "select cmis:name from dpp:person")]
  (map get-property-by-query-name (take 5 qr)))

;; SELECT * FROM dpp:collection where dpp:collectionType = 'product' order by cmis:lastModificationDate desc
;; http://localhost:8080/share/page/folder-details?nodeRef=workspace://SpacesStore/1d08a3ce-9da2-498f-bc77-c4e76bf3a4ef
(let [qr (query alfresco-session false "select cmis:objectId from dpp:collection where dpp:collectionType = 'product' and cmis:parentId = 'workspace://SpacesStore/1d08a3ce-9da2-498f-bc77-c4e76bf3a4ef' order by cmis:lastModificationDate desc")]
  (map get-property-by-name qr))

(let [qr (query alfresco-session false "SELECT dpp:status, cmis:objectId  from dpp:article  where cmis:name='1200002'")]
  (map get-property-by-status qr))


(comment
  (let [qr (query in-mem-session false "select cmis:name, cmisbook:author from cmisbook:lyrics")]
    (map get-property-by-query-name (take 5 qr))))

(comment
  (let [qr (query in-mem-session false "select cmis:name from cmisbook:lyrics")]
    (map get-property-by-query-name qr)))

(comment
  (query in-mem-session false "select cmis:objectId from cmisbook:lyrics"))

(defn test-fn [result]
  (. (. result getPropertyByQueryName "cmis:objectId") getFirstValue))

(comment
(let [qr (query alfresco-session false "select cmis:objectId from cmis:document")]
  (map test-fn (seq qr)))
)

(comment (count (query->seq alfresco-session "select cmis:objectId from cmis:document")))

(comment
(first (query->seq alfresco-session "select dpp:personId from dpp:person"))
(. (. (first (query->seq alfresco-session "select cmis:objectId from cmis:document")) getPropertyByQueryName "cmis:objectId") getFirstValue)
(. (. (first (query->seq alfresco-session "select dpp:personId from dpp:person")) getPropertyByQueryName "dpp:personId") getDisplayName)
  )

(comment
(type (query->seq alfresco-session "SELECT cmis:objectId from dpp:article where cmis:name='1189880'"))
(type (query alfresco-session false "SELECT cmis:objectId from dpp:article"))
  )

(comment
(let [qr (query alfresco-session false "select cmis:objectId from cmis:document")]
  (map test-fn (seq qr)))
  )

(comment
(count (query->seq alfresco-session "select cmis:objectId from cmis:document"))
  )

(comment
(count (query->seq alfresco-session "select cmis:objectId from cmis:folder"))
  )

(comment
(let [qr (query alfresco-session false "select cmis:objectId from cmis:folder")]
  (map test-fn (seq qr))))

(comment
(let [qr (query alfresco-session false "select cmis:name from dpp:person")]
  (map get-property-by-query-name (take 5 qr)))
  )

;; SELECT * FROM dpp:collection where dpp:collectionType = 'product' order by cmis:lastModificationDate desc
;; http://localhost:8080/share/page/folder-details?nodeRef=workspace://SpacesStore/1d08a3ce-9da2-498f-bc77-c4e76bf3a4ef
(comment
  (let [qr (query alfresco-session false "select cmis:objectId from dpp:collection where dpp:collectionType = 'product' and cmis:parentId = 'workspace://SpacesStore/1d08a3ce-9da2-498f-bc77-c4e76bf3a4ef' order by cmis:lastModificationDate desc")]
  (map get-property-by-name qr))
  )

(comment
(let [qr (query alfresco-session false "SELECT dpp:status, cmis:objectId  from dpp:article  where cmis:name='1189880'")]
  (map get-property-by-status qr))
  )

(comment
(let [qr (query alfresco-session false "SELECT cmis:objectId from dpp:article")]
  (map get-property-by-status qr))
)
