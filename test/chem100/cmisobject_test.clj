(ns chem100.cmisobject-test
  (import org.apache.chemistry.opencmis.client.api.Folder)
  (import org.apache.chemistry.opencmis.client.api.Document)
  (import org.apache.chemistry.opencmis.client.api.SessionFactory)
  (:require [chem100.session :as session]
            [chem100.folder :as folder]
            [chem100.cmisobject :as co])
  (:use midje.sweet))

(def in-mem-session
  (session/create-session "http://localhost:8081/inmemory/atom/" "test" "test" "A1"))

(comment
  (fact "getContentStreamLength" (. (get-object test-session "310") getContentStreamLength) => 395)
  (fact "is-folder" (instance? Folder (get-object test-session "100")) => true)
  (fact "SessionFactory" (instance? SessionFactory (session-factory)) => true)
  )

(with-state-changes [(around :facts (let [test-session (session/create-session "http://localhost:8081/inmemory/atom/" "test" "test" "A1")] ?form))]
                    (fact "get-object using a string" (instance? Document (co/get-object test-session "310")) => true)
                    (fact "get root folder" (instance? Folder (. test-session folder/getRootFolder)) => true)
                    (fact "get-object using an object" (instance? Document (co/get-object test-session (co/create-object-id test-session "310"))) => true)
                    )
