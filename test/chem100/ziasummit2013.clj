(ns chem100.ziasummit2013
  (import org.apache.chemistry.opencmis.client.api.ItemIterable)
  (import org.apache.chemistry.opencmis.client.api.Folder)
  (:require [chem100.session :as session]
            [chem100.repository :as repo]
            [chem100.folder :as folder]
            [chem100.document :as doc]
            [chem100.cmisobject :as co]))
(use '[clojure.string :only (join split)])

;; let's connect to our local repo
(def repo-param (session/create-parameter "http://localhost:8080/alfresco/cmisatom" "admin" "admin"))
(println repo-param)

;; let's create a session
(def alfresco-session
  (session/create-session repo-param (repo/get-first-repository-id repo-param)))
(println alfresco-session)

(. alfresco-session folder/getRootFolder)

(time (folder/get-root-folder alfresco-session))
;(time (folder/create-folder (folder/get-root-folder alfresco-session) "My Test Folder123"))

(time (folder/get-children (folder/get-root-folder alfresco-session)))
(time (folder/get-children (folder/get-root-folder alfresco-session)))

;; ------------------------------------------------------------------------
;; let's define some functions
(defn query [session search-all-versions query]
  (. session query query search-all-versions))

(defn get-property-by-query-name [result]
  (. (. result getPropertyByQueryName "cmis:name") getFirstValue))

(defn get-property-by-name [result]
  (. (. result getPropertyByQueryName "cmis:objectId") getFirstValue))

(defn get-property-by-person-id [result]
  (. (. result getPropertyByQueryName "dpp:personId") getFirstValue))

(defn get-property [field result]
  (. (. result getPropertyByQueryName field) getFirstValue))

(defn build-query [field type where-clause]
  (str "select " field " from " type " where " where-clause))

(println
  (build-query "dpp:personId" "dpp:person" "dpp:personId like 'generic%'") )

(defn create-object-id [object]
  (co/create-object-id alfresco-session
                       ( first
                         (split  object #";"))) )

(defn delete-object [object]
    (. object delete))

;; ------------------------------------------------------------------------
;; let's see select persondId for all -- raw cmis query
;(println
;  (let [qr (query alfresco-session false "select cmis:objectId from dpp:person where dpp:personId like 'a%'")]
;    (map get-property-by-name qr)))

;; let's get a count
;(println
;  (count
;    (let [qr (query alfresco-session false "select dpp:personId from dpp:person")]
;      (map get-property-by-person-id qr))))

;; lets see the first 5 returned
;(println
;  (let [qr (query alfresco-session false "select dpp:personId from dpp:person")]
;    (map get-property-by-person-id (take 5 qr))))

;; let's see the first 5 sorted
;(println
;  (sort
;    (let [qr (query alfresco-session false "select dpp:personId from dpp:person")]
;      (map get-property-by-person-id (take 5 qr)))))

;; ------------------------------------------------------------------------
;; let's define a function a Field like dpp:personId, a Type like dpp:person and a where-clause
(defn run-cmis-query
  ([field type where-clause]
     (let [sql-query (build-query field type where-clause)
           qr (query alfresco-session false sql-query)]
       (map (partial get-property field) qr))))

(time (run-cmis-query "cmis:name" "cmis:folder" "cmis:name like '%Test%'"))

(run-cmis-query "cmis:name" "cmis:folder" "cmis:name like '%Test%'")
;; let's see the the g's
;(println (run-cmis-query "dpp:personId" "dpp:person" "dpp:personId not like 'generic%'"))

;; let's see the the not g's sorted
;(println (sort (run-cmis-query "dpp:personId" "dpp:person" "dpp:personId not like 'generic%'")))

;; let's see the the g's lastName sorted
;(println (sort (run-cmis-query "dpp:lastName" "dpp:person" "dpp:personId like 'g%'")))

;; let's see the the g's firstName sorted
;(println (sort (run-cmis-query "dpp:firstName" "dpp:person" "dpp:personId like 'g%'")))

;; let's see the the g's as ObjectIds/NodeRefs
;(println (run-cmis-query "cmis:objectId" "dpp:person" "dpp:personId like 'g%'"))

;; let's see the g's as Objects
;(println
;  (map (partial co/create-object-id alfresco-session)
;    ( run-cmis-query "cmis:objectId" "dpp:person" "dpp:personId like 'g%'")))

;; ------------------------------------------------------------------------
;; let's see the the a's sorted
;(println (sort (run-cmis-query "dpp:personId" "dpp:person" "dpp:personId like 'a%'")))

;; let's get a count of the a's
;(println (count (run-cmis-query "dpp:personId" "dpp:person" "dpp:personId like 'a%'")))

;; let's see a...
;(println (run-cmis-query "dpp:personId" "dpp:person" "dpp:personId not like 'generic%'"))

;; let's see a count
;(println (count (run-cmis-query "dpp:personId" "dpp:person" "dpp:personId not like 'generic%'")))

;; let's see them as Objects
(println
  (map (partial co/create-object-id alfresco-session)
    ( run-cmis-query "cmis:name" "cmis:folder" "cmis:name not like 'generic%'")))

;ItemIterable<CmisObject> childObjectItrble = rootFolder.getChildren();
;Iterator<CmisObject> childObjectItr = childObjectItrble.iterator();

(comment
(println (time
(.
   (co/get-object
    alfresco-session
    (first
     (map (partial  co/create-object-id alfresco-session)
          (run-cmis-query "cmis:objectId" "dpp:person" "dpp:personId not like 'generic%'")))) delete))))

;(co/get-object
;    alfresco-session
;    (first
;     (map (partial  co/create-object-id alfresco-session)
;          (run-cmis-query "cmis:objectId" "dpp:person" "dpp:personId not like 'generic%'"))))
(comment
(println
(time
(map (partial co/delete alfresco-session )
(seq
  (map (partial co/create-object-id alfresco-session)
          (run-cmis-query "cmis:objectId" "dpp:person" "dpp:personId not like 'generic%' and dpp:legacyPersonId is not null")))))))

;; let's see a...
;(println (run-cmis-query "dpp:personId" "dpp:person" "dpp:personId like 'adam%'"))

;(println (sort (run-cmis-query "dpp:manuscriptNumber" "dpp:article" "dpp:manuscriptNumber is not null")))
;(println (count (run-cmis-query "dpp:manuscriptNumber" "dpp:article" "dpp:manuscriptNumber is not null")))
;; let's see the the 1184961 as ObjectIds/NodeRefs
;(run-cmis-query "dpp:manuscriptNumber" "dpp:article" "dpp:manuscriptNumber = 1184961")

(comment
(map (partial co/delete alfresco-session )
  (seq
    (map (partial co/create-object-id alfresco-session)
          (run-cmis-query "cmis:objectId" "dpp:article" "dpp:manuscriptNumber = 1184961")))))


