(ns chem100.alfrescoSummit2013.demo001)

;; Keyboard mappings...
;; shift-ctrl-w
;; shift-ctrl-delete
;; ctrl-space
;; ctrl-f
;; This isn't the REPL, rather this can be connected to a REPL
;; evaluate a form - ctrl-enter
;; evaluate the whole file - shift-ctrl-enter...
;; this is a comment

(+ 3 4)
(+ 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16)
(* 1 2 3)

(+ 1 2 3 4 5)
(range 1 17)
(take 5 (range 1 17))

(def numbers '(1 57 43 2 3 8 32))
numbers

(sort numbers)
numbers

(count numbers)

(flatten '(("Hello") ("World")))

(def test-map {:last "Stang" :first "Mark"})
test-map
(keys test-map)
(vals test-map)
(sort test-map)
test-map

(def strings (list "Hello" "World"))
strings

(type "Hello World")
(defn to-lower-case
  [string]
  (. string toLowerCase))
(to-lower-case "HELLO")

(defn to-upper-case
  "This function converts a string to uppercase."
  [string]
  (. string toUpperCase))
(to-upper-case "hello world")

(map to-upper-case strings)
(map to-lower-case strings)
strings

(println "Hello World")
(println (+ 3 4))
