(ns chem100.mandatory
  (import org.apache.chemistry.opencmis.client.api.ItemIterable)
  (import org.apache.chemistry.opencmis.client.api.Folder)
  (:require [chem100.session :as session]
            [chem100.repository :as repo]
            [chem100.folder :as folder]
            [chem100.document :as doc]
            [chem100.cmisobject :as co]))
(use '[clojure.string :only (join split)])

;; let's connect to our local repo
(def repo-param (session/create-parameter "http://localhost:8080/alfresco/cmisatom" "admin" "admin"))
(println repo-param)

;; let's create a session
(def alfresco-session
  (session/create-session repo-param (repo/get-first-repository-id repo-param)))
(println alfresco-session)

;; ------------------------------------------------------------------------
;; let's define some functions
(defn query [session search-all-versions query]
  (. session query query search-all-versions))

(defn get-property-by-query-name [result]
  (. (. result getPropertyByQueryName "cmis:name") getFirstValue))

(defn get-property-by-name [result]
  (. (. result getPropertyByQueryName "cmis:objectId") getFirstValue))

(defn get-property [field result]
  (. (. result getPropertyByQueryName field) getFirstValue))

(defn create-object-id [object]
  (co/create-object-id alfresco-session
                       ( first
                         (split  object #";"))) )

(defn delete-object [object]
    (. object delete))

(defn delete-folder [folder]
    (folder/delete-tree folder))

;;(get-property-by-name (co/get-object-by-path alfresco-session "/"))
(println
  (let [qr (query alfresco-session false "select cmis:objectId, cmis:name from cmis:folder where cmis:name like 'Comp%'")]
    (map get-property-by-name qr)))

(co/get-name (co/get-object alfresco-session "workspace://SpacesStore/b31514be-75c2-4998-b009-4544433cff5c"))

(co/get-properties (co/get-object alfresco-session "workspace://SpacesStore/b31514be-75c2-4998-b009-4544433cff5c"))

(co/get-name (co/get-object-by-path alfresco-session "/"))

(delete-object (co/get-object-by-path alfresco-session "/assignedTech"))

(delete-folder (co/get-object-by-path alfresco-session "/customer"))

(defn create-folder-props [folder-name]
  (doto (new java.util.HashMap)
    (.put "cmis:objectTypeId" "F:sched:customerFolder")
    (.put "cmis:name" folder-name)
    (.put "sched:customerLastName", "Johnson")))
(defn create-doc-props [document-name]
  (doto (new java.util.HashMap)
    (.put "cmis:objectTypeId" "D:sched:assignedTech")
    (.put "cmis:name" document-name)
    ;(.put "sched:techLastName", "Johnson")
    ))
(folder/create-folder-with-props (co/get-object-by-path alfresco-session "/") (create-folder-props "customer"))
(doc/create-document-with-props (co/get-object-by-path alfresco-session "/") (create-doc-props "assignedTech"))

