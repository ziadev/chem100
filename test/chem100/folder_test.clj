(ns chem100.folder-test
  (import org.apache.chemistry.opencmis.client.api.Folder)
  (import org.apache.chemistry.opencmis.client.api.Document)
  (import org.apache.chemistry.opencmis.client.api.SessionFactory)
  (:require  [chem100.session :as session]
             [chem100.cmisobject :as co]
             [chem100.folder :as folder])
  (:use midje.sweet
        [clojure.string :only (join split)]))

;(map co/get-object-id (. (folder/get-root-folder in-mem-session) getChildren))
;(map co/get-properties (. (folder/get-root-folder in-mem-session) getChildren))

;(map find-class-name (supers org.apache.chemistry.opencmis.client.runtime.FolderImpl))

;(folder/create-folder-context in-mem-session)

(comment
  (co/get-name (first
                (get-page
                 (get-children
                  (session/get-object in-mem-session "100" (create-folder-context in-mem-session)))
                 2
                 3) )))

;(co/get-name (first (get-parents (co/get-object in-mem-session "101"))))
;(first (get-parents (co/get-object in-mem-session "100")))

(with-state-changes [(around :facts (let [test-session (session/create-session "http://localhost:8081/inmemory/atom/" "test" "test" "A1")] ?form))]
  (fact "root folder" (instance? Folder (folder/get-root-folder test-session)) => true)
  (fact "getChildren" (instance? Document (last (. (folder/get-root-folder test-session) getChildren))) => true)
  (fact "count Children" (count (co/get-properties (last (. (folder/get-root-folder test-session) getChildren)))) => 24)
  (fact "bean test" (bean (last (. (folder/get-root-folder test-session) getChildren))) => (contains {:acl nil :versionSeriesId "311"}))
  (fact "count the characters" (count (last (split "org.apache.chemistry.opencmis.client.runtime.DocumentImpl" #"\."))) => 12)
  (fact "first object-id" (first (map co/get-object-id (. (folder/get-root-folder test-session) getChildren))) => "101")
  (fact "create folder" (instance? Folder (folder/create-folder  (folder/get-root-folder test-session) "My Test Folder")) => true)
  (fact "created?" (instance? Folder (co/get-object-by-path test-session "/My Test Folder")) => true)
  (fact "delete My Test Folder" (. (co/get-object-by-path test-session "/My Test Folder") delete) => nil)
  )

