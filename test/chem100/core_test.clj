(ns chem100.core-test
  (:use clojure.test
        chem100.core)
  (:require [chem100.session :as session]))

(def test-session
  (session/create-session "http://localhost:8081/inmemory/atom/" "test" "test" "A1"))

(deftest a-test
  (testing "FIXME, I fail."
    (is (= 0 1))))

(defn one-time-setup []
  (println "one time setup")
  (println "line 2 of one time setup")
)
 
(defn one-time-teardown []
  (println "one time teardown"))
 
(defn once-fixture [f]
  (one-time-setup)
  (f)
  (one-time-teardown))
 
;; register as a one-time callback
(use-fixtures :once once-fixture)

(run-tests 'chem100.core-test)
